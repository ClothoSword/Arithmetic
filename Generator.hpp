#pragma once
#include "NumberExpression.hpp"
#include "AdditionExpression.hpp"
#include "MultiplicationExpression.hpp"
#include "SubtractionExpression.hpp"
#include "DivisionExpression.hpp"
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>

using namespace std;

class Generator{
public:	
	Generator(){
		srand((unsigned)time(NULL));
	}
	
	void generate(int begin,int end){
		
		int num = 0;
		cout<<"请输入要生成的题目数量"<<endl; 
		cin>>num;
		
		Expression* ea;
		Expression* eb;
		Expression* ex;
		Expression* ey;
		
		float operator_choice = 0.0;
		float fraction_choice = 0.0;
		
		ofstream out_question("question.txt"),out_answer("answer.txt");  
		
		for(int i=0;i<10;i++){

			operator_choice = rand_float();
			fraction_choice = rand_float();
			if(fraction_choice<0.5){
				if(operator_choice<0.25){
					ea = new AdditionExpression(new NumberExpression(rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end)));
				}else if(operator_choice<0.5){
					ea = new SubtractionExpression(new NumberExpression(rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end)));
				}else if(operator_choice<0.75){
					ea = new MultiplicationExpression(new NumberExpression(rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end)));
				}else{
					ea = new DivisionExpression(new NumberExpression(rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end)));
				}
			}
			else{
				if(operator_choice<0.25){
					ea = new AdditionExpression(new NumberExpression(rand_int_range(begin,end),rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end)));
				}
				else if(operator_choice<0.5){
					ea = new SubtractionExpression(new NumberExpression(rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end),rand_int_range(begin,end)));
				}else if(operator_choice<0.75){
					ea = new MultiplicationExpression(new NumberExpression(rand_int_range(begin,end),rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end)));
				}else{
					ea = new DivisionExpression(new NumberExpression(rand_int_range(begin,end)),new NumberExpression(rand_int_range(begin,end),rand_int_range(begin,end)));
				}
			}
			
			ex = ea->get_value_o();
			
			operator_choice = rand_float();
			fraction_choice = rand_float();
			Expression* t_1 = new NumberExpression(rand_int_range(begin,end));
			Expression* t_2 = new NumberExpression(rand_int_range(begin,end),rand_int_range(begin,end));
			if(fraction_choice<0.75){
				if(operator_choice<0.25){
					eb = new AdditionExpression(ea,t_1);
					ey = new AdditionExpression(ex,t_1);
				}
				else if(operator_choice<0.5){
					eb = new SubtractionExpression(ea,t_1);
					ey = new SubtractionExpression(ex,t_1);
				}else if(operator_choice<0.75){
					eb = new MultiplicationExpression(ea,t_1);
					ey = new MultiplicationExpression(ex,t_1);
				}else{
					eb = new DivisionExpression(ea,t_1);
					ey = new DivisionExpression(ex,t_1);
				}
			}else{
				if(operator_choice<0.25){
					eb = new AdditionExpression(ea,t_2);
					ey = new AdditionExpression(ex,t_2);
				}
				else if(operator_choice<0.5){
					eb = new SubtractionExpression(ea,t_2);
					ey = new SubtractionExpression(ex,t_2);
				}else if(operator_choice<0.75){
					eb = new MultiplicationExpression(ea,t_2);
					ey = new MultiplicationExpression(ex,t_2);
				}else{
					eb = new DivisionExpression(ea,t_2);
					ey = new DivisionExpression(ex,t_2);
				}
			}
			
			
			vector<char> temp = eb->evaluate();
			for(char i : temp){
			if(i=='+'||i=='-'||i=='*'||i=='\\'||i=='('||i==')'||i=='^'){
				out_question<<i;
			}
			else 
				out_question<<(int)i;
			}
			out_question<<"="<<'\n';
			
			temp = ey->get_value_o()->evaluate();
			for(char i : temp){
			if(i=='+'||i=='-'||i=='*'||i=='\\'||i=='('||i==')'||i=='^'){
				out_answer<<i;
			}
			else 
				out_answer<<(int)i;
			}
			out_answer<<'\n';
		}
		out_question.close();
		out_answer.close();
		cout<<"生成完毕，请在本地文件查看题目和答案"<<endl; 
	}
	
	int rand_int_range(int begin,int end){
		int value = rand()%(end-begin+1)+begin;
		return value;
	}
	
	float rand_float(void){
		return ((float)rand()/(float)RAND_MAX);
	}
	
};

 
