#pragma once
#include "Expression.hpp"
#include <iostream>
using namespace std;

class NumberExpression : public Expression
{
public:
	NumberExpression(int v_a):v_actual(v_a){
		v_actual_denom = 0;
		v_fraction_int = 0;
		is_fraction = false;
	}
	
	NumberExpression(int v_a,int v_a_d){
		int common_division = find_common_divison(v_a,v_a_d);
		v_a/=common_division;
		v_a_d/=common_division;
		if(v_a_d==1){
			is_fraction = false;
		}else{
			is_fraction = true;
		}
		int quotient = v_a/v_a_d;
		int remainder = v_a%v_a_d;
		
		if(quotient!=0)
			v_fraction_int = quotient;
		else
			v_fraction_int = 0;
		v_actual = remainder;
		v_actual_denom = v_a_d;
	}	
	
	virtual vector<char> evaluate(){
		if(!is_fraction)
			v_literal.push_back(v_actual);
		else{	
			if(v_fraction_int!=0&&v_actual!=0){
				v_literal.push_back('(');
				v_literal.push_back(v_fraction_int);
				v_literal.push_back('^');
				v_literal.push_back(v_actual);
				v_literal.push_back('\\');
				v_literal.push_back(v_actual_denom);
				v_literal.push_back(')');
			}
			else if(v_actual_denom==1){
				v_literal.push_back(v_actual);
			}
			else{
				v_literal.push_back('(');
				v_literal.push_back(v_actual);
				v_literal.push_back('\\');
				v_literal.push_back(v_actual_denom);
				v_literal.push_back(')');
			}
		}
		return v_literal;
	}
	
	virtual vector<char> get_value(){
		if(!is_fraction)
			value.push_back(v_actual);
		else{
			if(v_fraction_int!=0&&v_actual!=0){
				value.push_back('(');
				value.push_back(v_fraction_int);
				value.push_back('^');
				value.push_back(v_actual);
				value.push_back('\\');
				value.push_back(v_actual_denom);
				value.push_back(')');
			}
			else if(v_actual_denom==1){
				value.push_back(v_actual);
			}
			else{
				value.push_back('(');
				value.push_back(v_actual);
				value.push_back('\\');
				value.push_back(v_actual_denom);
				value.push_back(')');
			}
		}
		return value;
	}
	
	bool isFraction(){
		return is_fraction;
	}
	int get_v_actual(){
		return v_actual;
	}
	int get_v_actual_denom(){
		return v_actual_denom;
	}
	int get_v_fraction_int(){
		return v_fraction_int;
	}
	void set_v_actual(int v){
		v_actual = v;
	}
	void set_v_actual_denom(int v){
		v_actual_denom = v;
	}
	void set_v_fraction_int(int v){
		v_fraction_int = v;
	}

private:
	int v_actual;  //整数或分子 
	int v_actual_denom;	//分母 
	int v_fraction_int;	//带分数整数值 
	bool is_fraction;

};
