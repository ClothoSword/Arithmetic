obj = main.o Expression.o AdditionExpression.o SubtractionExpression.o MultiplicationExpression.o DivisionExpression.o NumberExpression.o Generate.o
inc = -I "./"

Arithmatic : $(obj)
	g++ $(obj) -o Arithmatic
main.o : main.cpp
	g++ -c main.cpp $(inc)
Expression.o : Expression.cpp
	g++ -c Expression.cpp $(inc)
AdditionExpression.o : AdditionExpression.cpp
	g++ -c AdditionExpression.cpp $(inc)
SubtractionExpression.o : SubtractionExpression.cpp
	g++ -c SubtractionExpression.cpp $(inc)
MultiplicationExpression.o : MultiplicationExpression.cpp
	g++ -c MultiplicationExpression.cpp $(inc)
DivisionExpression.o : DivisionExpression.cpp
	g++ -c DivisionExpression.cpp $(inc)
NumberExpression.o : NumberExpression.cpp
	g++ -c NumberExpression.cpp $(inc)
Generate.o : Generate.cpp
	g++ -c Generate.cpp $(inc)


.PHONY:clean
clean : 
	rm *.o *.exe