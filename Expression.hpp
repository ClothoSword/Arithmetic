#pragma once
#include <vector>
#include <cmath>

using namespace std; 

class Expression
{
public:
	virtual ~Expression(){};
	virtual vector<char> evaluate() = 0;
	virtual vector<char> get_value(){};
	virtual Expression* get_value_o(){};
	virtual bool isFraction(){}
	virtual int get_v_actual(){}
	virtual int get_v_actual_denom(){}
	virtual int get_v_fraction_int(){}
	virtual	void set_v_actual(int v){}
	virtual void set_v_actual_denom(int v){}
	virtual void set_v_fraction_int(int v){}
	
protected:
	vector<char> v_literal;
	vector<char> value; 
	
	
	int find_common_divison(int a,int b){
		int temp;
		if(a<b)
		{
			temp=a;
			a=b;
			b=temp;
		}
		while(b!=0)
		{
			temp=a%b;
			a=b;
			b=temp;
		}
		
		int value = a; 		
		return value;		
	}
	
	int find_common_multiple(int a,int b){
		int temp;
		temp=a*b/find_common_divison(a,b);
		return temp;	
	}

};
