#pragma once
#include "Expression.hpp"

class AdditionExpression : public Expression{
public:
	AdditionExpression(Expression* left,Expression* right):left_(left),right_(right){}
	
	virtual vector<char> evaluate(){
		vector<char> temp;
		temp = left_->evaluate();
		temp.push_back('+');
		for(char a:(right_->evaluate())){
			temp.push_back(a);
		}
		return temp;
	}
	
	virtual Expression* get_value_o(){
		return add_fraction(left_,right_);
	} 
	
private:
	Expression* left_;
	Expression* right_;
	
	Expression* add_fraction(Expression* a,Expression* b){
		if(a->isFraction()&&b->isFraction()){
			int fraction_int = a->get_v_fraction_int()+b->get_v_fraction_int();
			int common_multiple = find_common_multiple(a->get_v_actual_denom(),b->get_v_actual_denom());
			a->set_v_actual(b->get_v_actual_denom()*a->get_v_actual());
			b->set_v_actual(a->get_v_actual_denom()*b->get_v_actual());
			int temp_actual = a->get_v_actual()+b->get_v_actual()+fraction_int*common_multiple;
			return (new NumberExpression(temp_actual,common_multiple));
		}
		else if(!a->isFraction()&&!b->isFraction()){
			return (new NumberExpression(a->get_v_actual()+b->get_v_actual()));
		}else{
			if(a->isFraction()){
				if(a->get_v_actual_denom()!=0){
					a->set_v_actual(a->get_v_actual_denom()*a->get_v_fraction_int()+a->get_v_actual());
				}
				b->set_v_actual(a->get_v_actual_denom()*b->get_v_actual());
				b->set_v_actual_denom(a->get_v_actual_denom());
				return (new NumberExpression(a->get_v_actual()+b->get_v_actual(),a->get_v_actual_denom()));
			}else{
				if(b->get_v_actual_denom()!=0){
					b->set_v_actual(b->get_v_actual_denom()*b->get_v_fraction_int()+b->get_v_actual());
				}
				a->set_v_actual(b->get_v_actual_denom()*a->get_v_actual());
				a->set_v_actual_denom(b->get_v_actual_denom());
				return (new NumberExpression(a->get_v_actual()+b->get_v_actual(),a->get_v_actual_denom()));
			}
		}
	}
};
